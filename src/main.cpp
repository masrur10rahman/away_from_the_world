#include <iostream>
#include <format>

int main() {
    std::string name{"Masrur Rahman"};
    std::cout << std::format("Hello world, I'm {}\n", name);
}
